﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Data.SqlClient;
using Site1.Models;
using System.Diagnostics;
using System.Threading;
using System.Web.Security;

namespace Site1.Controllers
{
    class PSRunner
    {
        public string Message = "";
        public bool IsRunning = false;
        public Process p;
        public PSRunner()
        { }

        public void Run(string psname, string arguments)
        {
            Message = "";
            p = new Process();
            p.StartInfo.Arguments = arguments;
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.FileName = psname;
            p.StartInfo.CreateNoWindow = false;
            p.StartInfo.RedirectStandardError = false;
            p.StartInfo.RedirectStandardInput = false;
            p.StartInfo.RedirectStandardOutput = true;
            p.OutputDataReceived += new DataReceivedEventHandler
            (
                delegate(object sender, DataReceivedEventArgs e)
                {
                    if (e != null && e.Data != null)
                        Message = e.Data.ToString();
                }
            );

            p.Start();

            IsRunning = true;
            p.BeginOutputReadLine();
            p.WaitForExit(1000);
            try
            {
                p.Kill();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                IsRunning = false;
            }
        }
    }

    public class InputController : Controller
    {
        //
        // GET: /Input/

       // [Authorize(Roles = "Admin, Student")]
        public ActionResult Student()
        {
            ViewBag.Message = "Вход для студентов";

            ServerModel sc = new ServerModel();
            IEnumerable<Site1.Models.solutions> Sol = sc.solutions;
            ViewData["ListSolution"] = Sol;
            return View();
        }

       // [Authorize(Roles = "Admin, Student")]
        public ActionResult Result()
        {
            ViewBag.Message = "Результаты";
            return View();
        }

       // [Authorize(Roles = "Admin, Teacher")]
        public ActionResult Teacher()
        {
            ViewBag.Message = "Вход для преподавателей";
            ServerModel sm = new ServerModel();
            IEnumerable<Site1.Models.solutions> Sol = sm.solutions;
            ViewBag.Solutions = Sol;
            return View();
        }

        //[Authorize(Roles = "Admin")]
        public ActionResult Admin()
        {
            ViewBag.Message = "Вход для администраторов";
            UsersContext uc = new UsersContext();
            IEnumerable<Site1.Models.UserProfile> Users = uc.UserProfiles.ToArray();
            ViewBag.Users = Users;
            return View();
        }

       // [Authorize(Roles = "Admin, Teacher")]
        [HttpPost]
        public ActionResult EditSolution(int SolId)
        {
            ServerModel sm = new ServerModel();
            Site1.Models.solutions sol = sm.solutions.First(x=>x.Id==SolId);
            ViewBag.Title = "Редактирование задания";
            ViewBag.Solution = sol;
            return View("EditSolution");
        }

       // [Authorize(Roles = "Admin, Teacher")]
        [HttpPost]
        public ActionResult ViewTests(int SolId)
        {
            ServerModel sm = new ServerModel();
            IEnumerable<Site1.Models.Tests> T = sm.Tests.Where(x => x.Solution == SolId).ToList();
            ViewBag.Title = "Просмотр тестов задания";
            ViewBag.Tests = T;
            ViewBag.Mess = sm.solutions.First(x => x.Id == SolId).Name;
            return View("ViewTests");
        }

       // [Authorize(Roles = "Admin, Student")]
        [HttpPost]
        public ActionResult ViewResults(int SolId)
        {
            ServerModel sm = new ServerModel();
            IEnumerable<Site1.Models.Result> R = sm.Result.Where(x => x.SolutionId == SolId).ToList();
            ViewBag.Title = "Просмотр результатов";
            ViewBag.Results = R;
            ViewBag.Mess = sm.solutions.First(x => x.Id == SolId).Name;
            return View("ViewResults");
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult UpdateSolution(int Id, string Name, string Description)
        {
            ServerModel sm = new ServerModel();
            Site1.Models.solutions source = sm.solutions.First(x => x.Id == Id);
            
            Site1.Models.solutions updated = new solutions();
            updated.Id = source.Id;
            updated.Name = Name;
            updated.Description = Description;

            sm.Entry(source).CurrentValues.SetValues(updated);
            sm.SaveChanges();

            return RedirectPermanent("Teacher");
        }

        [Authorize]
        [HttpPost]
        public ActionResult UploadFile(IEnumerable<HttpPostedFileBase> fileUpload)
        {
            if (fileUpload.Count() != 0)
            {
                int SolId = 3;
                HttpPostedFileBase file = fileUpload.First();

                string path = AppDomain.CurrentDomain.BaseDirectory + "UploadedFiles/";
                string filename = Path.GetFileName(file.FileName);
                string newFileName = String.Format("Thumbnail_{0}_{1}.exe", DateTime.Now.ToString("yyyyMMddHHmmssfff"), Guid.NewGuid());

                if (filename != null) file.SaveAs(Path.Combine(path, newFileName));

                ServerModel sc = new ServerModel();
                IEnumerable<Site1.Models.Tests> Tests = sc.Tests.Where(x => x.Solution == SolId).ToList();
                List<string> ListResult = new List<string>();
                List<bool> ListBoolResult = new List<bool>();
                foreach (Site1.Models.Tests t in Tests)
                {
                    PSRunner runner = new PSRunner();

                    runner.Run(Path.Combine(path, newFileName), t.Input);
                    Thread.Sleep(200);
                    if (runner.IsRunning)
                    {
                        try
                        {
                            runner.p.Kill();
                        }
                        catch
                        {
                            ListResult.Add(t.Input + " : Время вышло");
                            ListBoolResult.Add(false);
                        }
                    }
                    else if (runner.Message == "")
                    {
                        ListResult.Add(t.Input + " : Нет ответа");
                        ListBoolResult.Add(false);
                    }
                    else if (runner.Message != t.Output)
                    {
                        ListResult.Add(t.Input + " : " + runner.Message + " - Неверный ответ");
                        ListBoolResult.Add(false);
                    }
                    else
                    {
                        ListResult.Add(t.Input + " : " + runner.Message + " - Верный ответ");
                        ListBoolResult.Add(true);
                    }
                   // UsersContext uc = new UsersContext();
                   // int UserId = uc.UserProfiles.Where(x => x.UserName == User.Identity.Name).ToList().First().UserId;
                    Site1.Models.Result res = new Models.Result();
                    res.SolutionId = SolId;
                    res.TestId = t.Id;
                    res.UserId = User.Identity.Name;
                    res.Result1 = ListBoolResult.Last() ? "Successfully" : "Fail";
                    res.Date = DateTime.Now;
                    sc.Result.Add(res);              
                }
                sc.SaveChanges();
                ViewBag.Result = ListResult;
                ViewBag.BoolResult = ListBoolResult;
                System.IO.File.Delete(Path.Combine(path, newFileName));

                return View("Result");
            }
            else
            {
                ViewBag.Messange = "Файл не загружен!";
                return View("Student");
            }

        }

    }
}
