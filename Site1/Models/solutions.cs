namespace Site1.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class solutions
    {
        public solutions()
        {
            Result = new HashSet<Result>();
            Tests = new HashSet<Tests>();
        }

        public int Id { get; set; }

        [Column(TypeName = "ntext")]
        public string Name { get; set; }

        [Column(TypeName = "ntext")]
        public string Description { get; set; }

        public virtual ICollection<Result> Result { get; set; }

        public virtual ICollection<Tests> Tests { get; set; }
    }
}
