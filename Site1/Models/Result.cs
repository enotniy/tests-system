namespace Site1.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Result")]
    public partial class Result
    {
        public int Id { get; set; }

        public int SolutionId { get; set; }

        public int TestId { get; set; }

        [Required]
        [StringLength(56)]
        public string UserId { get; set; }

        [Column("Result", TypeName = "text")]
        public string Result1 { get; set; }

        public DateTime Date { get; set; }

        public virtual Tests Tests { get; set; }

        public virtual solutions solutions { get; set; }
    }
}
