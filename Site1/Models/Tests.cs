namespace Site1.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Tests
    {
        public Tests()
        {
            Result = new HashSet<Result>();
        }

        public int Id { get; set; }

        [Column(TypeName = "text")]
        [Required]
        public string Input { get; set; }

        [Column(TypeName = "text")]
        public string Output { get; set; }

        public int Solution { get; set; }

        public virtual ICollection<Result> Result { get; set; }

        public virtual solutions solutions { get; set; }
    }
}
