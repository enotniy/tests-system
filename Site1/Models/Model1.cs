namespace Site1.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model1 : DbContext
    {
        public Model1()
            : base("name=Model1")
        {
        }

        public virtual DbSet<Result> Result { get; set; }
        public virtual DbSet<solutions> solutions { get; set; }
        public virtual DbSet<Tests> Tests { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Result>()
                .Property(e => e.Result1)
                .IsUnicode(false);

            modelBuilder.Entity<solutions>()
                .HasMany(e => e.Result)
                .WithRequired(e => e.solutions)
                .HasForeignKey(e => e.SolutionId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<solutions>()
                .HasMany(e => e.Tests)
                .WithRequired(e => e.solutions)
                .HasForeignKey(e => e.Solution)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Tests>()
                .Property(e => e.Input)
                .IsUnicode(false);

            modelBuilder.Entity<Tests>()
                .Property(e => e.Output)
                .IsUnicode(false);

            modelBuilder.Entity<Tests>()
                .HasMany(e => e.Result)
                .WithRequired(e => e.Tests)
                .HasForeignKey(e => e.TestId)
                .WillCascadeOnDelete(false);
        }
    }
}
